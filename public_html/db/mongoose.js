const mongoose = require('mongoose');

// Connect and create new database propdirect
// Variable MONGODB_URI comes form mongolab and exactly is true for PORT
mongoose.connect(process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/propdirect', {
    useNewUrlParser: true, 
    useUnifiedTopology: true, 
    useCreateIndex: true,
    useFindAndModify: false,
    useMongoClient: true
});