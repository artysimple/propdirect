const { src, dest, watch } = require('gulp');
var sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

// Compile sass into css
function style() {
    return src('./resources/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('./resources/css'))
        // Stream changes to all browser
        .pipe(browserSync.stream())
}

// Watch sass files when change 
function watcher() {
    browserSync.init({
        server: {
           baseDir: './'
        }
    });
    watch('./resources/sass/**/*.scss', style);
    watch('./*.html').on('change', browserSync.reload);
}

module.exports = {
    style,
    watcher
}