/*
const formLogin = document.getElementById('formLogin');
const btnLogin = document.getElementById('btnLogin');

const setTokenLocalStorage = (value) => {
    if(value) {
        localStorage.setItem('authorization', value);
    }
}

const getTokenLocalStorage = () => localStorage.getItem('authorization');

// * local storage
const handleRegistration = () => {
    if(getTokenLocalStorage !== null) {
        $('#loginModal').modal('hide');
    }
}


const url = window.location.pathname;

var searchParams = new URLSearchParams(url);
if (searchParams.has('/users/user')) {
    
    fetch('/users/me', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + getTokenLocalStorage()
        }
    })
    .then((res) => res.json())
    .then((data) => {
        console.log(data)
        
        if (data.role === 'Customer') {
            return data;
        }
    })
}

const logoutOne = document.getElementById('logoutOne');
if (logoutOne) logoutOne.addEventListener('click', getLogoutOne)

const getLogoutOne = () => {
    const login = document.getElementById('login');
    const customerLogged = document.querySelector('#customerLogged');
    const sellerLogged = document.querySelector('#sellerLogged');
    var searchParams = new URLSearchParams(window.location.pathname);
    
    if (searchParams.has('/logout/me')) {
    
        fetch('/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + getTokenLocalStorage()
            }
        })
        .then((res) => {
            res.json()
            console.log('res');
        })
        .then((data) => {
            
            console.log('outside');
            localStorage.removeItem('authorization');
            if (data) {
                console.log('inside');
            }

            sellerLogged.innerHTML = '';
            customerLogged.innerHTML = '';

            const html = `
            <li class="nav-item account">
                <a href="/register" class="btn-primary">Creer un compte</a>
            </li>
            <li class="nav-item dashboard">
                    <a href="/login" class="btn-secondary">Se connecter</a>
            </li>`
            login.insertAdjacentHTML('afterbegin', html);
            window.location.href = '/login';
        })
        .catch(err => console.log(err))
    }
}

formLogin.addEventListener('submit', function (e) {
    e.preventDefault();
    const email = formLogin.email.value;
    const password = formLogin.password.value;

    fetch('/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password }),
    })
    .then((res) => res.json())
    .then((data) => {
        setTokenLocalStorage(data.token);

        if (data.user.role === 'Seller') {
            const login = document.querySelector('#login');
            const sellerLogged = document.querySelector('#sellerLogged');
            const html = `
            <li class="nav-item add-property">
                <a href="/properties/store" class="btn-primary">Ajouter un propriété</a>
              </li>

            <li class="dropdown dashboard">
            <button class="btn-secondary dropdown-toggle" type="button" id="zoneDashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Mon espace
            </button>
            <div class="dropdown-menu" aria-labelledby="zoneDashboard">
                <a class="dropdown-item" href="/dashboard-seller">Table de bord</a>
                <a class="dropdown-item" href="/logout/me" id="logoutOne">Logout</a>
                <a class="dropdown-item" href="/logoutAll">Logout from all devices</a>
            </div>
            </li>`
          login.innerHTML = '';
          sellerLogged.innerHTML = '';
          sellerLogged.insertAdjacentHTML('afterbegin', html);
        }

        if (data.user.role === 'Customer') {
            console.log('Customer');
        }
        // window.location.href = '/';
    })
    .catch((error) => {
        console.log(error);
        return error;
    })

    // const getLogin = async () => {
    //     const rawResponse = await fetch('/login', {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({ email, password })
    //     });
    //     const content = await rawResponse.json();

    //     if (!content.error) {
    //         formLogin.reset();
    //         $('#loginModal').modal('hide');
    //     }

    //     console.log(content);
    // }
    // getLogin();

    // axios.post('/users/login', {
    //     email,
    //     password
    //   })
    //   .then(function (response) {
    //     console.log(response);
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   });
});
*/